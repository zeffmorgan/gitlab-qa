# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '12.4.0'
  end
end
